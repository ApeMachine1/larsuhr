const express  = require('express');
const app = express();
const path = require('path');

app.get('/', function (req,res) {
  var options = {
    root: path.join(__dirname)
  };
  res.sendFile('index.html', options);
});

app.get('/start.js', function (req,res) {
  var options = {
    root: path.join(__dirname)
  };
  res.sendFile('start.js', options);
});

app.get('/style.css', function (req,res) {
  var options = {
    root: path.join(__dirname)
  };
  res.sendFile('style.css', options);
}); 

app.get('/font.ttf', function (req,res) {
  var options = {
    root: path.join(__dirname)
  };
  res.sendFile('font.ttf', options);
}); 

app.listen(3000);