const clock = document.getElementById("clock");
const clocknr = document.getElementById("clocknr");

const init = function() {
    var now = new Date().getTime();
    var hours = Math.floor((now % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)) + 1;
    var minutes = Math.floor((now % (1000 * 60 * 60)) / (1000 * 60));
    if(hours === 24 && minutes === 0){
      clocknr.innerHTML = "000";
      clock.innerHTML = "Wendepunkt der Zeit";
    }
    else if(hours === 12 && minutes === 0){
      clocknr.innerHTML = "ham";
      clock.innerHTML = "Mittagessen";
    }
    else if(hours >= 0 && hours < 12 ){
      var larstime = (hours * 60) + minutes;
      clocknr.innerHTML = larstime;
      clock.innerHTML = "seit gestern";
    }
    else{
      var larstime = ((24 - hours) * 60) - minutes;
      clocknr.innerHTML = larstime;
      clock.innerHTML = "bis morgen";
    }
}
//Update the count down every 1 second
init();
var x = setInterval(function() {
    init();
}, 1000);

